//var map = L.map('map').setView([51.505, -0.09], 13);

function onAccuratePositionProgress (e) {
    console.log(e.accuracy);
    console.log(e.latlng);
}

function onAccuratePositionFound (e) {
    console.log(e.accuracy);
    console.log(e.latlng);
}

function onAccuratePositionError (e) {
    console.log(e.message)
}

map.on('accuratepositionprogress', onAccuratePositionProgress);
map.on('accuratepositionfound', onAccuratePositionFound);
map.on('accuratepositionerror', onAccuratePositionError);

map.findAccuratePosition({
    maxWait: 15000, // defaults to 10000
    desiredAccuracy: 30 // defaults to 20
});
