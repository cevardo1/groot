var express = require('express');
var router = express.Router();
var config = require('dotenv').config();

// Mongoose Schema definition
var Schema = mongoose.Schema;
var JsonSchema = new Schema({
    name: String,
    type: Schema.Types.Mixed
});

// Mongoose Model definition
var Json = mongoose.model('JString', JsonSchema, 'layercollection');

/* GET json data. */
router.get('/mapjson/:name', function (req, res) {
    if (req.params.name) {
        Json.findOne({ name: req.params.name },{}, function (err, docs) {
            res.json(docs);
        });
    }
});

/* GET layers json data. */
router.get('/maplayers', function (req, res) {
    Json.find({},{'name': 1}, function (err, docs) {
        res.json(docs);
    });
});

/* GET Map page. */
router.get('/maps', function(req,res) {
    var db = req.db;
    Json.find({},{}, function(e,docs){
        res.render('maps', {
            "jmap" : docs,
            /* Manhattan, NY */
            lat : 40.78854,
            lng : -73.96374

            /* City of Mexico
            lat : 19.43236,
            lng : -99.13290
            */
        });
    });
});

module.exports = router;
