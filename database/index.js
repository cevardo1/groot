var express = require('express');
var router = express.Router();
var config = require('dotenv').config();
var async = require('async');
var MongoClient = require('mongodb').MongoClient;

// Mongoose import
var mongoose = require('mongoose');
var Promise = require('bluebird');

// use node A+ promises
mongoose.Promise = Promise;

// check for connection string
var mongoUrl = process.env.MONGO_URL;

if (!mongoUrl) {
  throw new Error('MONGO_URL env variable not set.');
}

var isConn;
// initialize MongoDB connection
if (mongoose.connections.length === 0) {
  mongoose.connect(mongoUrl);
} else {
  mongoose.connections.forEach(function(conn) {
    if (!conn.host) {
      isConn = false;
    }
  })

  if (isConn === false) {
    mongoose.connect(mongoUrl);
  }
}

// Mongoose Schema definition
var Schema = mongoose.Schema;
var JsonSchema = new Schema({
    name: String,
    type: Schema.Types.Mixed
});

// Mongoose Model definition
var Json = mongoose.model('JString', JsonSchema, 'layercollection');

module.exports = function (cb) {
  async.parallel(databases, cb);
};
