module.exports = {
    // HTTP port
    port: process.env.PORT || 3000,
    twilioPort: process.env.TWILIO_PORT || 1337,
    nodeEnv: process.env.NODE_ENV,
    // MongoDB connection string - MONGO_URL is for local dev,
    // MONGOLAB_URI is for the MongoLab add-on for Heroku deployment
    // mongoUrl: process.env.MONGOLAB_URI || process.env.MONGO_URL || process.env.MONGODB_URI
    mongoUrl: process.env.MONGO_URL
};
